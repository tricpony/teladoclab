//
//  SplitViewController.swift
//  TeladocLab
//
//  Created by aarthur on 8/28/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController, UISplitViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        preferredDisplayMode = .allVisible
        let minimumWidth = min(view.bounds.size.width, view.bounds.size.height)
        minimumPrimaryColumnWidth = minimumWidth / 2
        maximumPrimaryColumnWidth = minimumWidth
    }

    // MARK: - UISplitViewControllerDelegate
    
    func splitViewController(_ splitViewController: UISplitViewController,
                             collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController) -> Bool {
        return !isCollapsed
    }
}
