//
//  VMTableViewCell.swift
//  TeladocLab
//
//  Created by aarthur on 8/29/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

/// A protocol to unify all table cell types and pass the responsibility of filling in its content.
@objc protocol VMTableViewCell where Self: UITableViewCell {
    static func reuseIdentifier()
    func fillCell(_ model: Model?)
}
