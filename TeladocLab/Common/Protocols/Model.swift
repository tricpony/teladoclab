//
//  Model.swift
//  TeladocLab
//
//  Created by aarthur on 8/29/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation

/// This is a protocol for all model classes.
@objc protocol Model where Self: NSObject {
    var name: String { get set }
    var imageURLString: String { get set }
    var detail: Model? { get set }
    var imageData: Data? { get set }
    var hasImageData: Bool { get }
}
