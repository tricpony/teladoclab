//
//  TableViewer.swift
//  TeladocLab
//
//  Created by aarthur on 8/29/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

protocol TableViewer: UITableViewDelegate, UITableViewDataSource where Self: UIViewController {
    var tableView: UITableView! { get set }
    
    /// Contains the data displayed by tableView.
    var dataSource: [Model]? { get set }

    /// Set selection behavior, leaving cells selected in iPad.
    func clearsSelectionOnViewWillAppear()

    /// Determine the proper cell identifier.
    /// - Parameters:
    ///   - indexPath: Index path of the cell that we need an identifier for.
    /// - Returns: Cell identifier.
    func cellIdentifier(at indexPath: IndexPath) -> String

    /// Determine the proper cell to use next based on cellIdentifier.
    /// - Parameters:
    ///   - tableView: TableView of next cell.
    ///   - indexPath: Index path of next cell.
    /// - Returns: A new cell conforming to VMTableViewCell.
    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> VMTableViewCell?
}

extension TableViewer {
    func clearsSelectionOnViewWillAppear() {
        let clear = splitViewController?.isCollapsed ?? true
        if clear {
            guard let indexPath = tableView.indexPathForSelectedRow else { return }
            tableView.deselectRow(at: indexPath, animated: false)
        }
    }

    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> VMTableViewCell? {
        return tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(at: indexPath)) as? VMTableViewCell
    }
}
