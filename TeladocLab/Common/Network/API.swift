//
//  API.swift
//  TeladocLab
//
//  Created by aarthur on 8/28/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation

enum API_EndPoint {
    case master
    case detail(arg: String)

    /// Provide service address of case.
    var serviceAddress: String {
        switch self {
        case .master:
            return "\(API_BaseURL.base)/test/json.php"
        case .detail(let value):
            return "\(API_BaseURL.base)/test/json.php?name=\(value)"
        }
    }

}

/// Type to define base url strings.
private struct API_BaseURL {
    static let base = "http://dev.tapptic.com"
}
