//
//  BusyPanel.swift
//  TeladocLab
//
//  Created by aarthur on 8/28/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

@IBDesignable class BusyPanel: UIView, NibLoadable {
    @IBOutlet weak var busyLabel: PulsatingLabel!
    @IBOutlet weak var pinWheel: UIActivityIndicatorView!
    private var _hidden = false
    
    @IBInspectable var busyValue: String = "Loading..." {
        didSet {
            busyLabel.text = busyValue
        }
    }
    @IBInspectable var hides: Bool = true {
        didSet {
            _hidden = hides
        }
    }
    @IBInspectable var color: UIColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1) {
        didSet {
            backgroundColor = color
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // border
        layer.borderWidth = 0.5
        layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        layer.cornerRadius = 12.25
        layer.masksToBounds = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupFromNib()
    }
    
    func startAnimating() {
        pinWheel.startAnimating()
        busyLabel.isHidden = false
        busyLabel.beginAnimations()
    }
    
    func stopAnimating() {
        pinWheel.stopAnimating()
        busyLabel.endAnimations()
        busyLabel.isHidden = true
        isHidden = _hidden
    }
}
