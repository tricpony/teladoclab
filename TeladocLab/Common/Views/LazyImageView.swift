//
//  LazyImageView.swift
//  TeladocLab
//
//  Created by aarthur on 8/30/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

/// An image view that will load itself from the url in a Model protocol.
@IBDesignable class LazyImageView: UIView, NibLoadable {
    @IBOutlet weak var pinwheel: UIActivityIndicatorView!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // border
        layer.borderWidth = 0.5
        layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        layer.cornerRadius = 12.25
        layer.masksToBounds = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupFromNib()
    }

    @objc
    func prepareForReuse() {
        imageView.image = nil
    }
    
    @objc
    func performImageService(model: Model) {
        let urlString = model.imageURLString
        pinwheel.startAnimating()

        _ = ServiceManager.sharedService.startServiceAt(urlString: urlString) { [weak self] result in
            switch result {
            case .success(let payload):
                DispatchQueue.main.async {
                    self?.pinwheel.stopAnimating()
                    model.imageData = payload
                    self?.imageView.image = UIImage(data: payload)
                }
            case .failure(let error):
                debugPrint("*** Error: \(String(describing: error.errorDescription))")
            }
        }
    }
    
    @objc
    func fillImage(model: Model) {
        guard let imageData = model.imageData else { return }
        imageView.image = UIImage(data: imageData)
    }
}
