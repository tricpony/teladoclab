//
//  AppDelegate.swift
//  TeladocLab
//
//  Created by aarthur on 8/28/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = sb.instantiateInitialViewController()
        window?.rootViewController = rootVC
        return true
    }
}
