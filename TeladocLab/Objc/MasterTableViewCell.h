//
//  MasterTableViewCell.h
//  TeladocLab
//
//  Created by aarthur on 8/30/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MasterTableViewCell : UITableViewCell

+ (NSString*)reuseIdentifier;

@end

NS_ASSUME_NONNULL_END
