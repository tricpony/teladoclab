//
//  MasterTableViewCell.m
//  TeladocLab
//
//  Created by aarthur on 8/30/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

#import "MasterTableViewCell.h"
#import "TeladocLab-Swift.h"

@interface MasterTableViewCell () <VMTableViewCell>
@property (weak, nonatomic) IBOutlet LazyImageView *lazyImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end

@implementation MasterTableViewCell

+ (NSString*)reuseIdentifier {
    return NSStringFromClass(self);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    UIView *selectedBackground = [[UIView alloc] initWithFrame:self.contentView.bounds];
    
    selectedBackground.backgroundColor = [UIColor clearColor];
    self.selectedBackgroundView = selectedBackground;
    self.nameLabel.highlightedTextColor = [UIColor whiteColor];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    // normally these would be defined among a library of colors in the assets catalog
    self.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0.2392156869 green:0.6745098233 blue:0.9686274529 alpha:1];
    [self.nameLabel setHighlighted:highlighted];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // normally these would be defined among a library of colors in the assets catalog
    self.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0.4745098054 green:0.8392156959 blue:0.9764705896 alpha:1];
    [self.nameLabel setHighlighted:NO];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.lazyImageView prepareForReuse];
}

- (void)fillCell:(id<Model>)model {
    self.nameLabel.text = model.name;
    
    if (model.hasImageData) {
        [self.lazyImageView fillImageWithModel:model];
    } else {
        [self.lazyImageView performImageServiceWithModel:model];
    }
}

@end
