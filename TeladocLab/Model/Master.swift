//
//  Master.swift
//  TeladocLab
//
//  Created by aarthur on 8/28/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation

class Master: NSObject, Model, Decodable {
    var name: String
    var imageURLString: String
    var imageData: Data?
    var detail: Model?
    var hasImageData: Bool {
        return imageData != nil
    }

    private enum CodingKeys: String, CodingKey {
        case name
        case image
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.imageURLString = try container.decode(String.self, forKey: .image)
    }
}
