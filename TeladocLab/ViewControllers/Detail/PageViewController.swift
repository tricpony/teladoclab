//
//  PageViewController.swift
//  TeladocLab
//
//  Created by aarthur on 8/31/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation

/// Makes a new detail page.
struct PageFactory {
    static func createPage(model: Model?, selectedPage: IndexPath?) -> DetailViewController {
        let sb = UIStoryboard(name: "DetailViewController", bundle: nil)
        let controller = sb.instantiateViewController(identifier: "DetailScene") as DetailViewController
        controller.masterObject = model
        controller.selectedIndexPath = selectedPage
        return controller
    }
}

/// Supports horizontal swipping to change the detail screen.
class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    weak var masterViewController: MasterViewController!
    var modelObjects: [Model]?
    var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Detail"
        guard let model = modelObjects?[selectedIndexPath?.row ?? 0] else {
            // this is a placeholder view showing an empty selection for iPad
            let detailViewController = PageFactory.createPage(model: nil, selectedPage: selectedIndexPath)
            setViewControllers([detailViewController], direction: .forward, animated: false, completion: nil)
            return
        }
        let detailViewController = PageFactory.createPage(model: model, selectedPage: selectedIndexPath)
        dataSource = self
        delegate = self
        setViewControllers([detailViewController], direction: .forward, animated: false, completion: nil)
    }
        
    // MARK: UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentDetail = viewController as? DetailViewController else { return nil }
        let index = currentDetail.selectedIndexPath.row
        
        if index == 0 {
            return nil
        }
        selectedIndexPath = IndexPath(row: index - 1, section: 0)
        guard let model = modelObjects?[selectedIndexPath?.row ?? 0] else { return nil }
        let page = PageFactory.createPage(model: model, selectedPage: selectedIndexPath)
        return page
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentDetail = viewController as? DetailViewController else { return nil }
        guard let modelObjects = modelObjects else { return nil }
        let index = currentDetail.selectedIndexPath.row
        
        if index >= modelObjects.count - 1 {
            return nil
        }
        selectedIndexPath = IndexPath(row: index + 1, section: 0)
        let model = modelObjects[selectedIndexPath?.row ?? 0]
        let page = PageFactory.createPage(model: model, selectedPage: selectedIndexPath)
        return page
    }
    
    // MARK: UIPageViewControllerDelegate

    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        if completed {
            guard let currentDetail = viewControllers?.last as? DetailViewController else { return }
            if masterViewController != nil {
                // keep the master's selected cell in sync with each swipe
                masterViewController.updateTableViewSelection(to: currentDetail.selectedIndexPath)
            }
        }
    }
}
