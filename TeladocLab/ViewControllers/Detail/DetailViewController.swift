//
//  DetailViewController.swift
//  TeladocLab
//
//  Created by aarthur on 8/28/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    var masterObject: Model?
    var selectedIndexPath: IndexPath!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lazyImageView: LazyImageView!
    @IBOutlet weak var emptySelectionLabel: UILabel!
    
    var detailItem: Model? {
        didSet {
            fillView()
        }
    }

    deinit {
        // debug helper to make sure I am not leaking
        debugPrint("*** Free!!")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailItem = masterObject?.detail
            
        if masterObject == nil, splitViewController?.isCollapsed == false {
            // this is a placeholder for when nothing is selected in iPad size class
            emptySelectionLabel.isHidden = false
            nameLabel.isHidden = true
            lazyImageView.isHidden = true
            view.backgroundColor = #colorLiteral(red: 0.5704585314, green: 0.5704723597, blue: 0.5704649091, alpha: 1)
        } else {
            emptySelectionLabel.isHidden = true
            nameLabel.isHidden = false
            lazyImageView.isHidden = false
        }
    }
    
    func fillView() {
        guard let detailItem = detailItem else {
            performDetailService()
            return
        }
        nameLabel?.text = detailItem.name
        if detailItem.hasImageData {
            lazyImageView?.fillImage(model: detailItem)
        } else {
            lazyImageView?.performImageService(model: detailItem)
        }
    }
    
    // MARK: - Network

    /// Fetch the detail data.
    func performDetailService() {
        guard let name = masterObject?.name else { return }
        let urlString = API_EndPoint.detail(arg: name).serviceAddress
        _ = ServiceManager.sharedService.startServiceAt(urlString: urlString) { [weak self] result in
            switch result {
            case .success(let payload):
                let fetchedObject = JsonUtility<Detail>.parseJSON(payload)
                if let masterObject = self?.masterObject {
                    masterObject.detail = fetchedObject
                }
                DispatchQueue.main.async {
                    self?.detailItem = fetchedObject
                }
            case .failure(let error):
                ServiceManager.sharedService.cancel()
                self?.handleError(error)
            }
        }
    }
    
    /// Display error in alert panel containing two buttons, Retry and Cancel.
    /// - Parameters:
    ///   - error: The error to display the contents of.
    func handleError(_ error: ServiceError) {
        DispatchQueue.main.async { [weak self] in
            self?.presentAlert(title: "Alert", message: error.errorDescription, buttonTitles: ["Retry", "Cancel"], completion: { [weak self] selection in
                switch selection {
                case .buttonOne:
                    self?.performDetailService()
                default:
                    return
                }
            })
        }
    }
}

