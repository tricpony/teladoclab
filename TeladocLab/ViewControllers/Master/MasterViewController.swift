//
//  MasterViewController.swift
//  TeladocLab
//
//  Created by aarthur on 8/28/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController, TableViewer {
    var selectedIndexPath: IndexPath!
    @IBOutlet weak var busyPanel: BusyPanel!
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource: [Model]? {
        didSet {
            tableView.isHidden = false
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isHidden = true
        title = "Master"
        view.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        performMasterService()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear()
        super.viewWillAppear(animated)
    }

    /// keep the tableView's selected cell in sync with each page controller swipe.
    func updateTableViewSelection(to indexPath: IndexPath?) {
        if splitViewController?.isCollapsed == false {
            guard let indexPath = indexPath else { return }
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        }
    }
    
    // MARK: - Network

    /// Fetch the master data.
    func performMasterService() {
        busyPanel.startAnimating()
        _ = ServiceManager.sharedService.startServiceAt(urlString: API_EndPoint.master.serviceAddress) { [weak self] result in
            DispatchQueue.main.async {
                self?.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                self?.busyPanel.stopAnimating()
            }

            switch result {
            case .success(let payload):
                let fetchedObjects = JsonUtility<[Master]>.parseJSON(payload)
                DispatchQueue.main.async {
                    self?.dataSource = fetchedObjects
                }
            case .failure(let error):
                ServiceManager.sharedService.cancel()
                self?.handleError(error)
            }
        }
    }
    
    /// Display error in alert panel containing two buttons, Retry and Cancel.
    /// - Parameters:
    ///   - error: The error to display the contents of.
    func handleError(_ error: ServiceError) {
        DispatchQueue.main.async { [weak self] in
            self?.presentAlert(title: "Alert", message: error.errorDescription, buttonTitles: ["Retry", "Cancel"], completion: { [weak self] selection in
                switch selection {
                case .buttonOne:
                    self?.performMasterService()
                default:
                    return
                }
            })
        }
    }

    // MARK: - Segues

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if splitViewController?.isCollapsed == false {
            // prevent duplicate selection
            return tableView.indexPathForSelectedRow != selectedIndexPath
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                guard let dataSource = dataSource else { return }
                guard let pageController = (segue.destination as! UINavigationController).topViewController as? PageViewController else { return }
                pageController.selectedIndexPath = indexPath
                pageController.modelObjects = dataSource
                pageController.masterViewController = self
                selectedIndexPath = indexPath

                pageController.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                pageController.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    func cellIdentifier(at indexPath: IndexPath) -> String {
        return MasterTableViewCell.reuseIdentifier()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = nextCellForTableView(tableView, at: indexPath) else { return UITableViewCell() }
        let model = dataSource?[indexPath.row]
        cell.fillCell(model)
        return cell
    }
}
