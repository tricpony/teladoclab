//
//  MockModel.swift
//  TeladocLabTests
//
//  Created by aarthur on 8/31/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation
@testable import TeladocLab

class MockModel: NSObject, Model, Decodable {
    var name = "name"
    var imageURLString = "url"
    var detail: Model?
    var imageData: Data?
    var hasImageData = false

    private enum CodingKeys: String, CodingKey {
        case name
        case image
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.imageURLString = try container.decode(String.self, forKey: .image)
    }
}
