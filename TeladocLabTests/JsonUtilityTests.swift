//
//  JsonUtilityTests.swift
//  TeladocLabTests
//
//  Created by aarthur on 8/31/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import XCTest
@testable import TeladocLab

class JsonUtilityTests: XCTestCase {
    let modelArrayJson = "[{\"name\":\"3\",\"image\":\"imageUrlString\"}]"
    let modelDictinoaryJson = "{\"name\":\"3\",\"image\":\"imageUrlString\"}"

    func testParsingJsonArray() {
        let payload = Data(modelArrayJson.utf8)
        let modelObjects = JsonUtility<[MockModel]>.parseJSON(payload)
        XCTAssertTrue(type(of: modelObjects) == [MockModel]?.self)
    }
    
    func testParsingJsonSingle() {
        let payload = Data(modelDictinoaryJson.utf8)
        let modelObjects = JsonUtility<MockModel>.parseJSON(payload)
        XCTAssertTrue(type(of: modelObjects) == MockModel?.self)
    }
}
