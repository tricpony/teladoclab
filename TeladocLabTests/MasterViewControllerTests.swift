//
//  MasterViewControllerTests.swift
//  TeladocLabTests
//
//  Created by aarthur on 8/31/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import XCTest
@testable import TeladocLab

class MasterViewControllerTests: XCTestCase {
    var masterViewController: MasterViewController!

    override func setUpWithError() throws {
        let storyboard = UIStoryboard(name: "MasterViewController", bundle: nil)
        masterViewController = storyboard.instantiateViewController(withIdentifier: "MasterViewScene") as? MasterViewController
    }

    override func tearDownWithError() throws {
        masterViewController = nil
    }

    func testTitle() {
        let title = "My Title"
        masterViewController.title = title
        XCTAssertEqual(title, masterViewController.title)
    }

    func testTableViewRowsReferenced() {
        let expectation = XCTestExpectation(description: "Verify table data source.")
        let mockDelegate = MockMapViewDelegate(expectation: expectation)
        _ = masterViewController.view
        masterViewController.tableView.delegate = mockDelegate
        masterViewController.tableView.dataSource = mockDelegate
        wait(for: [expectation], timeout: 1.0)
        XCTAssertNotNil(masterViewController.dataSource)
    }
}

class MockMapViewDelegate: NSObject, UITableViewDataSource, UITableViewDelegate {
    var expectation: XCTestExpectation!
    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        expectation?.fulfill()
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
}
